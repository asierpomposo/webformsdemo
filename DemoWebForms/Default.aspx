﻿<%@ Page Title="Home Page" Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="DemoWebForms._Default" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>

   <h1> Default Page </h1>
            
    <h2> Links to pages with HDIV </h2>
     <hdiv:HDIVHyperlink ID="HDIVHyperlink2" runat="server" NavigateUrl="~/Pages/LoginHDIV.aspx">Simple Login</hdiv:HDIVHyperlink>
     <hdiv:HDIVHyperlink ID="HDIVHyperlink3" runat="server" NavigateUrl="~/Pages/LoginHDIVwithMaster.aspx">Master Login</hdiv:HDIVHyperlink>

     <h2> Links to pages without HDIV </h2>

      <asp:HyperLink ID="HDIVHyperlink4" runat="server" NavigateUrl="~/Pages/Login.aspx">Simple Login</asp:HyperLink>
      <asp:HyperLink ID="HDIVHyperlink5" runat="server" NavigateUrl="~/Pages/LoginwithMaster.aspx">Master Login</asp:HyperLink>

     <h2> Links to pages with parameters </h2>
        <hdiv:HDIVHyperlink ID="HyperLink1" runat="server" NavigateUrl="~/Contact.aspx/?var=foo&car2=foo2">HyperLink with HDIV</hdiv:HDIVHyperlink>
            <asp:HyperLink runat="server"  NavigateUrl="~/Contact.aspx/?var=foo">HyperLink without HDIV</asp:HyperLink>


    

    <form id="form1" runat="server">
    <div>
    
    </div>
    </form>
</body>
</html>
