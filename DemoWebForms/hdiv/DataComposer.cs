﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.SessionState;

namespace DemoWebForms.hdiv
{
    public class DataComposer
    {
        private const String DASH = "-";
        private int requestCounter = 0;
        private Stack<HDIVState> statesStack;
        private HDIVPage page;
        private HDIVSession session;
         

        public void init()
        {
            this.setPage(new HDIVPage());
            this.statesStack = new Stack<HDIVState>();
        }

        public void setPage(HDIVPage page)
        {
            this.page = page;
        }

        public void initPage()
        {
            this.page = new HDIVPage();
            string pageId = this.session.getPageId();
            this.page.setName(pageId);
            this.page.setRandomToken(UidGenerator.generateUid());            
           // this.page.setRandomToken(this.getMyRandom());
        }

        public void comprobar()
        {
            HttpSessionState Session = HttpContext.Current.Session;           
            HDIVPage page = (HDIVPage)Session[this.page.getName()];
            if (page != null)
            {
                System.Diagnostics.Debug.WriteLine("page Name :" + page.getName());
                System.Diagnostics.Debug.WriteLine("page states :" + page.getStates().Count);
            }
            else
            {
                System.Diagnostics.Debug.WriteLine("mierda");
            }
            
        }



        public string beginRequest(string action)
        {
            HDIVState state = new HDIVState(this.requestCounter);
            state.setAction(action);
            return this.beginRequest(state);
        }

        public String beginRequest(HDIVState state)
        {
            this.getStatesStack().Push(state);

            this.requestCounter = state.getId() + 1;

            String id = this.getPage().getName() + DASH + state.getId() + DASH + this.getHdivStateSuffix();
            return id;
        }


        public Stack<HDIVState> getStatesStack()
        {
            return this.statesStack;
        }
        public HDIVPage getPage()
        {
            return page;
        }

        protected String getHdivStateSuffix()
        {
            return this.getPage().getRandomToken();
        }

        public void setSession(HDIVSession session)
        {
            this.session = session;
        }

        public void startPage()
        {
            this.initPage();
        }




        public Parameter compose(string parameter, string value, Boolean editable)
        {
            return this.composeParameter(parameter, value, false);
        }


        protected Parameter composeParameter(string parameterName, string value, Boolean editable)
        {           

            // Get actual IState
            HDIVState state = this.getStatesStack().Peek();
          
            // create a new parameter and add to the request
            Parameter parameter = this.createParameter(parameterName, value, editable);
            state.addParameter(parameter);            

            return parameter;
        }

        protected Parameter createParameter(string parameterName, string value, Boolean editable)
        {
            return new Parameter(parameterName, value, editable);
        }


        private string getMyRandom()
        {
            var chars = "abcdefghijklmnopqrstuvwxyz0123456789";
            var random = new Random();
            var result = new string(
                Enumerable.Repeat(chars, 32)
                          .Select(s => s[random.Next(s.Length)])
                          .ToArray());

            return result;
        }

        public String endRequest()
        {

            HDIVState state = this.statesStack.Pop();

            HDIVPage page = this.getPage();
            state.setPageId(page.getName());
            page.addState(state);

            // Save Page in session if this is the first state to add
            Boolean firstState = page.getStatesCount() == 1;
            if (firstState)
            {
                this.session.addPage(page.getName(), page);
            }

            String id = this.getPage().getName() + DASH + state.getId() + DASH + this.getHdivStateSuffix();
            return id;
        }

        public void endPage()
        {
            if (this.isRequestStarted())
            {
                this.endRequest();
            }

            HDIVPage page = this.getPage();
            if (page.getStatesCount() > 0)
            {
                // The page has states, update them in session
                this.session.addPage(page.getName(), page);
            }
        }

        public Boolean isRequestStarted()
        {
            return this.statesStack.Count > 0;
        }


    }




     





}