﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DemoWebForms.hdiv
{
    public class HDIVPage
    {
        private string name;
        private string randomToken;
        private string flowId;
        private Dictionary<int, HDIVState> states = new Dictionary<int, HDIVState>();


        public void addState(HDIVState state)
        {
            this.states.Add(state.getId(), state);
        }

        public Boolean existState(int id)
        {
            return this.states.ContainsKey(id);
        }

        public HDIVState getState(int id)
        {
            return this.states[id];
        }

        public String getName()
        {
            return name;
        }

        public void setName(string name)
        {
            this.name = name;
        }

        public Dictionary<int, HDIVState>.ValueCollection getStates()
        {
            return this.states.Values;
        }

        public int getStatesCount()
        {
            return this.states.Count;
        }

        public String getFlowId()
        {
            return this.flowId;
        }

        public void setFlowId(String flowId)
        {
            this.flowId = flowId;
        }

        public String getRandomToken()
        {
            return this.randomToken;
        }

        public void setRandomToken(String randomToken)
        {
            this.randomToken = randomToken;
        }

        public string tosString()
        {
            string result = "Page:" + name + " ";
            foreach (HDIVState state in this.getStates())
            {
                result += " " + state.toString();
            }
            return result;
        }
    }
}