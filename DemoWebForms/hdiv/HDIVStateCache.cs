﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DemoWebForms.hdiv
{
    public class HDIVStateCache
    {
        private int maxSize;
        private List<string> pageIds = new List<string>();

        public HDIVStateCache()
        {
            this.setMaxSize(3);
        }

        public string addPage(string key)
        {
            if (this.pageIds.Contains(key))
            {
                // Page id already exist in session
                return null;
            }
            else
            {
                string removedKey = this.cleanBuffer();
                this.pageIds.Add(key);
                return removedKey;
            }
        }

        public string cleanBuffer()
        {
            if (this.pageIds.Count >= this.maxSize)
            {
                // delete first element
                string key = this.pageIds[0];
                this.pageIds.RemoveAt(0);
                return key;
            }
            return null;
        }

        public string tostring() {
		    string result = "[";
		    foreach (string pageId in pageIds) {
			    result += " " + pageId;
		    }
		    result += " ]";
		    return result;
	    }

        public int getMaxSize()
        {
            return maxSize;
        }

        public void setMaxSize(int maxSize)
        {
            this.maxSize = maxSize;
        }

        public List<string> getPageIds()
        {
            return pageIds;
        }
    }
}