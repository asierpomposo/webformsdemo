﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DemoWebForms.hdiv
{
    public class Parameter
    {
        private String name;
        private List<String> values = new List<String>();
        private Boolean editable;
        private int count = 0;

        public Parameter(String name, String value, Boolean editable)
        {
            this.name = name;
            this.addValue(value);
            this.editable = editable;
        }

        public void addValue(String value)
        {
            this.values.Add(value);
            this.count++;
        }

        public Boolean existValue(String value) {
		    foreach (String val in this.values) {
                if (val.Equals(value, StringComparison.InvariantCultureIgnoreCase))
                {
				    return true;
			    }
		    }
		    return false;
	    }

        public Boolean existPosition(int position)
        {
            return (position < this.values.Count);
        }

        public String getValuePosition(int position)
        {
            return this.values[position];
        }

        public String getName()
        {
            return this.name;
        }

        public void setName(String name)
        {
            this.name = name;
        }

        public List<String> getValues()
        {
            return this.values;
        }

        public void setValues(List<String> values)
        {
            this.values = values;
        }

        public Boolean isEditable()
        {
            return this.editable;
        }

        public void setEditable(Boolean editable)
        {
            this.editable = editable;
        }

        public String getConfidentialValue()
        {
            if (count == 0)
            {
                return "0";
            }
            return (count - 1).ToString();
        }

        public void setCount(int count)
        {
            this.count = count;
        }


        public String toString()
        {
            string result = " Parameter:" + this.getName() + " Values:";
            for (int i = 0; i < this.values.Count; i++)
            {
                String value = (String)this.values[i];
                result += value;
                if (!(i + 1 == this.values.Count))
                {
                    result += ",";
                }
            }
            return result;
        }
    }
}