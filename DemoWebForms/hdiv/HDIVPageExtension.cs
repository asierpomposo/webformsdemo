﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Collections;




namespace DemoWebForms.hdiv
{

    

    public class HDIVPageExtension : System.Web.UI.Page
    {

        private Stack<HDIVElement> requestFormElements;
        private Stack<HDIVElement> viewstateSavedElements3333;

        private ArrayList viewstateSavedElements;
        private int requestElementCounter = 0;
        private HDIVPage thisPage;

        // System.Diagnostics.Debug.WriteLine(; 

        protected override void OnInit(EventArgs e)
        {
            base.OnInit(e);
            if (!Page.IsPostBack)
            {   DataValidator validator = DataFactory.getValidator(HttpContext.Current);
                Boolean islegal = validator.isLegal();
                if (!islegal) Response.Redirect("~/Pages/HDIVUnauthorized.aspx");           
            }

          } // end onInit

        protected override void OnLoad(EventArgs e)
        {
            thisPage = new HDIVPage();
            generateHDIVHyperlink(this.Page);
        }

        protected override Object SaveViewState()
        {
            saveValueInformation(this.Page, 0);
            return base.SaveViewState();
        }

        protected override void LoadViewState(Object savedState)
        {
            this.requestFormElements = new Stack<HDIVElement>();
            this.viewstateSavedElements3333 = new Stack<HDIVElement>();
            this.viewstateSavedElements = new ArrayList();

            if (ViewState != null) base.LoadViewState(savedState);
            if (Page.IsPostBack)
            {
                generateUserNewSendElements(this.Page, 0);
                Boolean isLegal = elementValidation();
                if (!isLegal) Response.Redirect("~/Pages/HDIVUnauthorized.aspx");
            }


        } //END loadViewState




       
        //method to save elements from user Request
        private void generateUserNewSendElements(Control ctl, int tree)
        {
            //save request values
            foreach (string key in Request.Form.Keys)
            {
                if (!key.Equals("__VIEWSTATE") && !key.Equals("__VIEWSTATEGENERATOR") && !key.Equals("__EVENTVALIDATION") && !key.Equals("__EVENTTARGET") && !key.Equals("__EVENTARGUMENT"))
                {
                    string answer = nameCLeaning(key);
                    HDIVElement newElement = new HDIVElement(answer, Request.Form[key]);
                    requestFormElements.Push(newElement);
                }
            }

            //get viewstate saved elements
            foreach (DictionaryEntry item in ViewState)
            {
                string elementName = item.Key.ToString();
                HDIVElement elem = (HDIVElement)ViewState[elementName];
                viewstateSavedElements.Add(elem);          
            }

        } //END generateUserNewSendElements


        //method to compare tow HDIVElements
        public Boolean compare(HDIVElement savedElement, HDIVElement userElement)
        {   Boolean isEqual = false;
            if (savedElement.getType().Equals(userElement.getType())) return true;
            return isEqual;
        }

        //element validation
        private Boolean elementValidation()
        {
            Boolean isEqual = false;
            ArrayList myGroupsName = new ArrayList();

            foreach (HDIVElement elementInViewState in viewstateSavedElements)
            {
                if (elementInViewState.getType().Equals("RadioButton"))
                {
                    string parentGroup = elementInViewState.getParent();
                    if (!myGroupsName.Contains(parentGroup))
                    {
                        myGroupsName.Add(parentGroup);
                        ArrayList groupValues = getAllGroupValues(parentGroup, 0);
                        HDIVElement elemenInRequest = getElementinRequest(parentGroup);
                        if (groupValues.Contains(elemenInRequest.getValue())) requestElementCounter++;
                        else Response.Redirect("~/Pages/HDIVUnauthorized.aspx");
                    }
                }

                else if (elementInViewState.getType().Equals("DropDownList"))
                {
                    string parentGroup = elementInViewState.getParent();

                    if (!myGroupsName.Contains(parentGroup))
                    {
                        myGroupsName.Add(parentGroup);
                        ArrayList groupValues = getAllGroupValues(parentGroup, 1);
                        HDIVElement elemenInRequest = getElementinRequest(parentGroup);
                        if (groupValues.Contains(elemenInRequest.getValue())) requestElementCounter++;
                        else Response.Redirect("~/Pages/HDIVUnauthorized.aspx");
                    }
                }

                else if (elementInViewState.getType().Equals("CheckBoxList"))
                {
                    string parentGroup = elementInViewState.getParent();


                    if (!myGroupsName.Contains(parentGroup))
                    {
                        ArrayList groupValues = getAllGroupValues(parentGroup, 1);
                        ArrayList requesElements = getElementSinRequest(parentGroup);
                        IEnumerator e = requesElements.GetEnumerator();
                        Boolean isOk = true;
                        while (e.MoveNext() && isOk)
                        {
                            HDIVElement rqElement = (HDIVElement)e.Current;
                            if (!groupValues.Contains(rqElement.getValue())) isOk = false;
                            else requestElementCounter++;
                        }
                        if (isOk) myGroupsName.Add(parentGroup);
                        else Response.Redirect("~/Pages/HDIVUnauthorized.aspx");
                    }
                }

                else if (elementInViewState.getType().Equals("System.Web.UI.WebControls.Button")) requestElementCounter++;

                else
                {

                    HDIVElement elemenInRequest = getElementinRequest(elementInViewState.getName());

                    if (elemenInRequest.Equals(null)) Response.Redirect("~/Pages/HDIVUnauthorized.aspx");
                    else
                    {
                        requestElementCounter++;
                        if (elementInViewState.getValue() == null)
                        {
                            DataValidator validator = DataFactory.getValidator(HttpContext.Current);
                            Boolean correct = validator.whiteList(elemenInRequest.getValue());
                            if (!correct)
                            {   System.Diagnostics.Debug.WriteLine("SQL INJECTION ATACK");
                                Response.Redirect("~/Pages/HDIVUnauthorized.aspx");
                            }
                        }
                        else
                        {
                            if (elementInViewState.getValue().Equals(elemenInRequest.getValue()))
                            {
                                DataValidator validator2 = DataFactory.getValidator(HttpContext.Current);
                                Boolean correct = validator2.whiteList(elemenInRequest.getValue());
                                if (!correct)
                                {
                                    System.Diagnostics.Debug.WriteLine("SQL INJECTION ATACK");
                                    Response.Redirect("~/Pages/HDIVUnauthorized.aspx");
                                }
                            }
                            else Response.Redirect("~/Pages/HDIVUnauthorized.aspx");
                        }
                    }
                }
            }

            if (requestElementCounter.Equals(requestFormElements.Count)) isEqual = true;
            return isEqual;
        }


        //method to get radioButton and Dropdown groups for validation
        private ArrayList getAllGroupValues(string groupName, int option)
        {
            ArrayList groupValues = new ArrayList();
            IEnumerator e = viewstateSavedElements.GetEnumerator();
            int cont =0 ;
                      
            while (e.MoveNext()) {
            HDIVElement viewStatetElem =(HDIVElement) e.Current;
                if (viewStatetElem.getParent().Equals(groupName))
                {                   
                    if(option==0) groupValues.Add(viewStatetElem.getName());
                    else groupValues.Add(viewStatetElem.getValue());
                }
                cont++;            
            }
            return groupValues;
        } //end getAllGroupValues

        // method to get ONe element from the Request
        private HDIVElement getElementinRequest(string key)
        {
            HDIVElement elem = null;
            foreach (HDIVElement requestElem in requestFormElements)
            {
                if (requestElem.getName().Equals(key))
                {
                    return requestElem;
                }


            }
            return elem;
        }

        // method to get More that one element from the Request
        private ArrayList getElementSinRequest(string key)
        {
            ArrayList allElements = new ArrayList();
            foreach (HDIVElement requestElem in requestFormElements)
            {
                if (requestElem.getName().Equals(key))
                {
                    allElements.Add(requestElem);
                }

            }
            return allElements;
        }


        private HDIVElement getElementinVS(string requestName)
        {
             HDIVElement elem= null;
             foreach (DictionaryEntry item in ViewState)
             {

                 string elementName = item.Key.ToString();
                 if (elementName.Equals(requestName))
                 {
                     elem = (HDIVElement)ViewState[elementName];
                     return elem;
                 }
             }
               return elem;                
        }
        


   

     
                
  
       

        
     
        
        
        //generates HDIVHyperlink
        private void generateHDIVHyperlink(Control ctl)
        {
            DataComposer composer = DataFactory.getComposer();
            //inicializar la página del momento               
            composer.startPage();

            foreach (Control c in ctl.Controls)
            {   if (c.GetType().ToString().Equals("ASP.site_master")) generateHDIVHyperlinkMastered(c);
                else
                {
                    if (c.GetType() == typeof(HDIVHyperlink))
                    {
                        string actionState = ((HDIVHyperlink)(c)).getAction();
                        // crea el state + guarda la "action" del state
                        string hdiv_state = composer.beginRequest(actionState);
                        //recorrer el hyperlink en busca de parámetros
                        Dictionary<string, String> d = ((HDIVHyperlink)(c)).getParametersFirstTime();
                        foreach (var pair in d)
                        {
                            string varName = pair.Key;
                            string varValue = pair.Value;
                            //guardar la información de cada paramétro
                            composer.compose(varName, varValue, false);
                        }
                        composer.endRequest();
                        //crear el HDIV_STATE para el hyperlink
                        ((HDIVHyperlink)(c)).setHdivState(hdiv_state);
                    }
                }
            }
            composer.endPage();
        } //end generateHDIVHyperlink

        //generates HDIVHyperlink when there is a Master Page
        private void generateHDIVHyperlinkMastered(Control ctl)
        {
            foreach (Control c in ctl.Controls)
            {
                if (c.GetType() != typeof(HDIVHyperlink)) generateHDIVHyperlinkMastered(c);
                else
                {
                    DataComposer composer = DataFactory.getComposer();
                    //inicializar la página del momento
                    composer.startPage();
                    string actionState = ((HDIVHyperlink)(c)).getAction();
                    // crea el state + guarda la "action" del state
                    string hdiv_state = composer.beginRequest(actionState);
                    //recorrer el hyperlink en busca de parámetros
                    Dictionary<string, String> d = ((HDIVHyperlink)(c)).getParametersFirstTime();
                    foreach (var pair in d)
                    {
                        string varName = pair.Key;
                        string varValue = pair.Value;
                        //guardar la información de cada paramétro
                        composer.compose(varName, varValue, false);
                    }
                    composer.endRequest();
                    //crear el HDIV_STATE para el hyperlink
                    ((HDIVHyperlink)(c)).setHdivState(hdiv_state);

                    composer.endPage();
                }
            }
        } //end generateHDIVHyperlinkMastered

    
        public HDIVElement createElement(Control c,string classType)
        {             
            string value = null;
            string parent = null;
            string  name = c.ID;
            
            if(classType.Equals("HiddenField")){            
            value = ((HiddenField)(c)).Value;
            Control pr = ((HiddenField)(c)).Parent;
            parent = pr.ID;
            }

              HDIVElement element = new HDIVElement(classType, name, value, parent);
            return element;        
        } 


        //saveInformation into ViewState
        public void saveValueInformation(Control ctl, int tree)
        {

            foreach (Control c in ctl.Controls)
            {
                if (c.GetType().ToString().Equals("ASP.site_master"))  saveValueMastered(this.Page, tree + 1, null);           

                else{

                    if (c.GetType() == typeof(HtmlForm)) { string htID = ((HtmlForm)(c)).ID; }
                    
                    if (!c.GetType().ToString().Contains("LiteralControl") && !c.GetType().ToString().Contains("HtmlControls"))
                    {
                        Control pr = ((Control)(c)).Parent;
                        string parent = pr.ID;

                        if (pr.GetType() == typeof(HtmlForm))
                        {
                            if (c.GetType() == typeof(HiddenField))
                            {
                                HDIVElement element = createElement(c, "HiddenField");
                                string vsName = element.getName();
                                ViewState.Add(vsName, element);
                            }

                            else if (c.GetType() == typeof(RadioButton))
                            {
                                string type = "RadioButton";
                                string hiddenId = c.ID;
                                string groupId = ((RadioButton)(c)).GroupName;
                                string value = ((RadioButton)(c)).Text;
                                string newId = hiddenId;
                                parent = groupId;
                                
                                HDIVElement element = new HDIVElement(type, newId, value, parent);
                                string vsName = element.getName();
                                ViewState.Add(vsName, element);
                            } 
                            
                            else if (c.GetType() == typeof(DropDownList)){
                                ListItemCollection listBoxData = new ListItemCollection();
                                listBoxData = ((DropDownList)(c)).Items;
                                string type = "DropDownList";
                                parent = c.ID;                            

                                foreach (ListItem item in listBoxData)
                                {   string value = item.Value;
                                    string newId = item.Text;
                                    HDIVElement element = new HDIVElement(type, newId, value, parent);
                                    string vsName = element.getName();                                    
                                    ViewState.Add(vsName, element);
                                }
                            }

                            else if (c.GetType() == typeof(CheckBoxList))
                            {
                                ListItemCollection listBoxData = new ListItemCollection();
                                listBoxData = ((CheckBoxList)(c)).Items;
                                string type = "CheckBoxList";
                                parent = c.ID;

                                foreach (ListItem item in listBoxData)
                                {
                                    string value = item.Value;
                                    string newId = item.Text;
                                    HDIVElement element = new HDIVElement(type, newId, value, parent);
                                    string vsName = element.getName();
                                    ViewState.Add(vsName, element);
                                }
                            }                                                           
                            else
                            {   string type = c.GetType().ToString();
                                string hiddenId = c.ID;
                                HDIVElement element = new HDIVElement(type, hiddenId, parent);
                                string vsName = element.getName();
                                ViewState.Add(vsName, element);
                            }
                        }
                    }
                    saveValueInformation(c, tree + 1);
                }

            } 
        } //END saveValueInformation


        public void saveValueMastered(Control ctl, int tree, string formParent)
        {
            foreach (Control c in ctl.Controls)
            {
                if (c.GetType() == typeof(LiteralControl))
                {
                    string text = ((LiteralControl)(c)).Text;
                    Boolean contains = text.Contains("form");
                    Boolean containsId = text.Contains("id");
                    if (contains && containsId) formParent = this.searchFormID(text);
                    else if(tree==2) formParent="masterForm";                   
                }           

                else if (c.GetType() == typeof(HiddenField))
                {
                    string type = "HiddenField";
                    string hiddenId = c.ID;
                    string value = ((HiddenField)(c)).Value;
                    string parent = formParent;
                    HDIVElement element = new HDIVElement(type, hiddenId, value, parent);
                    string vsName = parent + "_" + hiddenId;
                    ViewState.Add(vsName, element);
                }

                else if (c.GetType() == typeof(RadioButton))
                {
                    string type = "RadioButton";
                    string hiddenId = c.ID;
                    string groupId = ((RadioButton)(c)).GroupName;
                    string newId = hiddenId;
                    string value = ((RadioButton)(c)).Text;
                    string parent = groupId;
                    HDIVElement element = new HDIVElement(type, newId, value, parent);
                    string vsName = element.getName();
                    ViewState.Add(vsName, element);
                }


               else if (c.GetType() == typeof(CheckBoxList))
                {
                    ListItemCollection listBoxData = new ListItemCollection();
                    listBoxData = ((CheckBoxList)(c)).Items;
                    string type = "CheckBoxList";
                    string parent = c.ID;
                    foreach (ListItem item in listBoxData)
                    {
                        string value = item.Value;
                        string newId = item.Text;
                        HDIVElement element = new HDIVElement(type, newId, value, parent);
                        string vsName = element.getName();
                        ViewState.Add(vsName, element);
                    }


                }
                else if (c.GetType() == typeof(DropDownList))
                {
                    ListItemCollection listBoxData = new ListItemCollection();
                    listBoxData = ((DropDownList)(c)).Items;
                    string type = "DropDownList";
                    string parent = c.ID;                   
                    foreach (ListItem item in listBoxData)
                    {
                        string value = item.Value;
                        string newId = item.Text;
                        HDIVElement element = new HDIVElement(type, newId, value, parent);
                        string vsName = element.getName();
                        ViewState.Add(vsName, element);
                    }

                }
                else {
                    if (c.Parent.GetType().ToString().Equals("System.Web.UI.WebControls.ContentPlaceHolder") && c.GetType()!=typeof(HDIVHyperlink))
                    {
                        string type = c.GetType().ToString();
                        string hiddenId = c.ID;
                        HDIVElement element = new HDIVElement(type, hiddenId, formParent);
                        string vsName = element.getName();
                        ViewState.Add(vsName, element);
                    } 
                }

                saveValueMastered(c, tree + 1, formParent);
            }

        } // SAVE VALUE MASTERED ENDS



        //cleans values content for validation 
        private string nameCLeaning(string textComplete)
        {
            string answer = null;
            string searchForThis0 = "$MainContent$";
            int lastCharacter0 = textComplete.IndexOf(searchForThis0);

            if (lastCharacter0 != -1) return answer= masterNameCleaning(textComplete, lastCharacter0);
            else
            {
                string searchForThis = "$";
                int lastCharacter = textComplete.IndexOf(searchForThis);
                if (lastCharacter != -1 && !textComplete.EndsWith("%"))
                {   for (int i = 0; i < lastCharacter; i++)
                    { answer = answer + textComplete[i]; }
                    return answer;
                }
            }
             return textComplete;
        }

        //cleans values content for validation when masterPage
        private string masterNameCleaning(string textComplete,int pos)
        {
            string answer = null;
            pos = pos + 12;
            if (!textComplete.EndsWith("%"))
            { 
                for (int i = pos+1; i < textComplete.Length; i++)
                {
                    answer = answer + textComplete[i];
                }
                return nameCLeaning(answer);
            }            
            return textComplete;
        }


        // searches for form id when theres a master
        private string searchFormID(string textComplete)
        {

            string answer=null;
            string searchForThis = "id=";
            int firstCharacter = textComplete.IndexOf(searchForThis);

            for (int i =  firstCharacter + 4 ; i <= textComplete.Length; i++) { 
                 if (textComplete[i] =='"' )  return answer;
                 else answer = answer + textComplete[i];
             }
             return answer;

        } // End searchFormID






    }


   

    
} ///CLASS ENDS