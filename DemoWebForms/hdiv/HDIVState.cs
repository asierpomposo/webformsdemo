﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DemoWebForms.hdiv
{
    public class HDIVState
    {
        private int id;
        private string action;
        private Dictionary<string, Parameter> parameters = new Dictionary<string, Parameter>();
        private string pageId;

        public HDIVState(int id)
        {
            this.id = id;
        }

        public void addParameter(Parameter parameter)
        {
            String paramName = parameter.getName();
            this.parameters.Add(paramName, parameter);
        }

        public Boolean existParameter(String key)
        {
            return this.parameters.ContainsKey(key);
        }

        public Parameter getParameter(String key)
        {
            try
            {
                return this.parameters[key];
            }
            catch (KeyNotFoundException)
            {
                return null;
            }
        }

        public String getAction()
        {
            return this.action;
        }

        public void setAction(String action)
        {
            this.action = action;
        }

        public Dictionary<string, Parameter>.ValueCollection getParameters()
        {
            return this.parameters.Values;
        }

        public void setParameters(Dictionary<string, Parameter> parameters)
        {
            this.parameters = parameters;
        }

        public int getId()
        {
            return this.id;
        }

        public String getPageId()
        {
            return this.pageId;
        }

        public void setPageId(String pageId)
        {
            this.pageId = pageId;
        }
        
        public String toString()
        {
            string result = "id: " + this.id;
            result += "action: " + this.action;
            result += "parameters: " + this.parameters;
            result += "pageId: " + this.pageId;
            return result;
        }
    }
}