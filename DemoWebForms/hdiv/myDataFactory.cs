﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DemoWebForms.hdiv
{
    public class myDataFactory
    {
      private static DataComposer composer;

        public static DataComposer getComposer()
        {
            if (composer != null) return composer;
            composer = new DataComposer();
            composer.init();
           // composer.setSession(new SessionHDIV());
            return composer;
        }
    }
}