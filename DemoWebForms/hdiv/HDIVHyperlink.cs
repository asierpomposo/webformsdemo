﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;

namespace DemoWebForms.hdiv
{
    public class HDIVHyperlink : System.Web.UI.WebControls.HyperLink
    {
        private string hdivState="_HDIV_STATE_=";
        private string hasForm = "_hasForm_=";
        private string originalURL;
        private Dictionary<string, string> parameters = new Dictionary<string, string>();

        public HDIVHyperlink()
        {
        }
        


        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            this.originalURL = this.NavigateUrl;

            if (hasParameters()) this.NavigateUrl += "&" + hdivState;
            
            else this.NavigateUrl +=  "/?" + hdivState;
        }

        
       public void setHdivState(string pageId, string stateId, string randomToken)
        {
            this.hdivState += pageId + "-" +  stateId +"-" +  randomToken;
        }

        public void setHdivState(string hdivState)
        {
            this.hdivState +=hdivState;
        }

        public string getHdivState()
        {
            return this.hdivState;
        }



        public string getAction()
        {
            string action = null;
            string searchForThis = "?";
            int numberOfCharacter = this.NavigateUrl.IndexOf(searchForThis);

            if (numberOfCharacter == -1) {  action = this.NavigateUrl; }

            else { 
                for (int i = 0; i < numberOfCharacter-1; i++)
                {  
                    action += this.NavigateUrl[i]; }
            }
            return action;
         }

        public Dictionary<string, string> getParametersFirstTime()
        {
            this.checkParameters();
            return this.parameters;
        }

        public Dictionary<string, string> getParameters()
        {
            return this.parameters;
        }


        private void checkParameters()
        {
            if (hasParameters()) 
            {
                Boolean hasValue = false;
                string myParam=null;
                string myValue=null;
                string searchForThis = "?";
                int firstCharacter = this.NavigateUrl.IndexOf(searchForThis);
               
                for (int i = firstCharacter + 1; i <= this.NavigateUrl.Length-1; i++)
                {
                    if (this.NavigateUrl[i].Equals('&')) {
                        hasValue=false;
                        parameters.Add(myParam, myValue);
                        myParam = null;
                        myValue = null;
                    }
                    else if ((this.NavigateUrl[i].Equals('='))){ hasValue=true; }
                    
                    else if(hasValue){  myValue= myValue + this.NavigateUrl[i]; }

                    else { myParam = myParam + this.NavigateUrl[i]; }
                }
                parameters.Add(myParam, myValue);
            }
            int count = parameters.Count();
        }



        private Boolean hasParameters()
        {
            return this.NavigateUrl.Contains("?");
        }


    }


}