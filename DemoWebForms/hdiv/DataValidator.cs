﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


using System.Collections.Generic;
using System.Configuration;
using System.Text.RegularExpressions;

namespace DemoWebForms.hdiv
{
    public class DataValidator
    {
        private HttpContext HttpContext;
        private string page;
        private string state;
        private string random;



        public DataValidator(HttpContext HttpContext)
        {
            this.HttpContext = HttpContext;
           // this.processHDIVState(HttpContext.Request["_HDIV_STATE_"]);

            if (HttpContext.Request["_HDIV_STATE_"] != null)
            {
                    this.processHDIVState(HttpContext.Request["_HDIV_STATE_"]);
            }



        }


        public Boolean isLegal()
        {
            if (isStartPage()) return true;
            if (!hasHDIVState()) return false;
            HDIVPage page = (HDIVPage)HttpContext.Session[this.page];

            if (page == null || !page.getRandomToken().Equals(this.random))
            { 
                return false;
            } 

            int stateid = Convert.ToInt32(this.state);
            if (!page.existState(stateid))
            {
                System.Diagnostics.Debug.WriteLine("error en state"); 
                return false;
            }


            HDIVState state = page.getState(stateid);
            Parameter param;

           

            if (state.getParameters().Count != HttpContext.Request.QueryString.Count + HttpContext.Request.Form.Count - 1) return false;
           
          
            foreach (string key in HttpContext.Request.QueryString.AllKeys.Concat(HttpContext.Request.Form.AllKeys))
            {

               
                if (key.Equals("_HDIV_STATE_")) continue;
                if (!state.existParameter(key)) return false;
                param = state.getParameter(key);
                String[] value = HttpContext.Request.QueryString.GetValues(key) ?? new String[0];
                if(!param.getValues().Contains(String.Concat(value))) return false;

             
            }
            return true;
        }

        private Boolean hasHDIVState()
        {
            return random != null;
        }
        private string getPageId()
        {
            return this.page;
        }
        private string getStateId()
        {
            return this.state;
        }
        private string getRandomToken()
        {
            return this.random;
        }

        private Boolean isStartPage()
        {
            Regex r = new Regex(ConfigurationManager.AppSettings["HDIV:StartPage"]);


            Boolean x = r.IsMatch(HttpContext.Request.Url.PathAndQuery);

            return r.IsMatch(HttpContext.Request.Url.PathAndQuery);
            //return HttpContext.Request.Url.PathAndQuery.Equals("/");
        }


       

        public Boolean whiteList(string inputString)
        {
            Regex r = new Regex("^[a-zA-Z0-9]+$");
            if (r.IsMatch(inputString))
                return true;
            else
                return false;
        }



        public void processHDIVState(string HDIVState)
        {
            try
            {
                string[] parts = HDIVState.Split('-');
                if (parts.Count() == 3)
                {
                    this.page = parts[0];
                    this.state = parts[1];
                    this.random = parts[2];
                }
            }
            catch (NullReferenceException) { }
        }

    }
}