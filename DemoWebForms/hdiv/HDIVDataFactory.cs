﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DemoWebForms.hdiv
{
    public static class DataFactory
    {
        private static DataComposer composer;
        private static DataValidator validator;

        public static DataComposer getComposer()
        {
            if (composer != null) return composer;
            composer = new DataComposer();
            composer.init();
            composer.setSession(new HDIVSession());
            return composer;
        }
       public static DataValidator getValidator(HttpContext HttpContext)
        {
            //if (validator != null) return validator;
            validator = new DataValidator(HttpContext);
            return validator;
        }
        public static DataValidator getValidator()
        {
            return validator;
        }
    }
}