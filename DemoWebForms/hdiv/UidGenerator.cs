﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Security.Cryptography;
using System.Text;
using System.Web.Routing;


namespace DemoWebForms.hdiv
{
    public static class UidGenerator
    {
        public static string generateUid()
        {
            string uniqid = System.Guid.NewGuid().ToString();
            byte[] encodedPassword = new UTF8Encoding().GetBytes(uniqid);
            byte[] hash = ((HashAlgorithm)CryptoConfig.CreateFromName("MD5")).ComputeHash(encodedPassword);
            return BitConverter.ToString(hash).Replace("-", string.Empty).ToLower();
        }
    }
}