﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Routing;
using System.Web.SessionState;

namespace DemoWebForms.hdiv
{
    public class HDIVSession
    {
        private String cacheName = "org.hdiv.StateCache";//Constants.STATE_CACHE_NAME;
        private String pageIdGeneratorName = "org.hdiv.PageIdGenerator";//Constants.PAGE_ID_GENERATOR_NAME;
        private String keyName = "org.hdiv.Key";//Constants.KEY_NAME;

        public String getPageId()
        {
            HttpSessionState Session = HttpContext.Current.Session;

            PageIdGenerator pageIdGenerator = (PageIdGenerator)Session[this.pageIdGeneratorName];
            
            if (pageIdGenerator == null)
            {
                pageIdGenerator = new PageIdGenerator();
            }

            String id = pageIdGenerator.getNextPageId();
            Session[this.pageIdGeneratorName] = pageIdGenerator;

            return id;

        }

        public HDIVPage getPage(String pageId)
        {
            HttpSessionState Session = HttpContext.Current.Session;
            return this.getPageFromSession(Session, pageId);

           
        }

        public void addPage(String pageId, HDIVPage page)
        {  

            HttpSessionState Session = HttpContext.Current.Session;

            HDIVStateCache cache = this.getStateCache(Session);

            page.setName(pageId);
            String removedPageId = cache.addPage(pageId);

            // if it returns a page identifier it is because the cache has reached
            // the maximum size and therefore we must delete the page which has been
            // stored for the longest time
            if (removedPageId != null)
            {this.removePageFromSession(Session, removedPageId);

            }

            // we update page identifier cache in session
            this.saveStateCache(Session, cache);

            // we add a new page in session
            this.addPageToSession(Session, page);

        }

        public void removeEndedPages(String conversationId)
        {

            HttpSessionState Session = HttpContext.Current.Session;

            HDIVStateCache cache = this.getStateCache(Session);
            //if (log.isDebugEnabled()) {
            //    log.debug("Cache pages before finished pages are deleted:" + cache.toString());
            //}

            List<string> pageIds = cache.getPageIds();

            for (int i = 0; i < pageIds.Count; i++)
            {

                string pageId = pageIds[i];
                HDIVPage currentPage = this.getPageFromSession(Session, pageId);
                if ((currentPage != null) && (currentPage.getFlowId() != null))
                {

                    String pageFlowId = currentPage.getFlowId();

                    if (conversationId.Equals(pageFlowId, StringComparison.InvariantCultureIgnoreCase))
                    {

                        this.removePageFromSession(Session, pageId);
                        pageIds.RemoveAt(i);
                        i--;
                    }
                }
            }

            //if (log.isDebugEnabled()) {
            //    log.debug("Cache pages after finished pages are deleted:" + cache.toString());
            //}
        }

        public HDIVState getState(String pageId, int stateId)
        {

            //try {
            HDIVPage currentPage = this.getPage(pageId);
            return currentPage.getState(stateId);

            //} catch (Exception e) {
            //    throw new HDIVException(HDIVErrorCodes.PAGE_ID_INCORRECT, e);
            //}
        }

        protected HDIVPage getPageFromSession(HttpSessionState Session, String pageId)
        {

            //if (log.isDebugEnabled()) {
            //    log.debug("Getting page with id:" + pageId);
            //}

            return (HDIVPage)Session[pageId];
        }

        protected void addPageToSession(HttpSessionState Session, HDIVPage page)
        {

            Session[page.getName()] = page;

            //if (log.isDebugEnabled()) {
            //    log.debug("Added new page with id:" + page.getName());
            //}
        }

        protected void removePageFromSession(HttpSessionState Session, String pageId)
        {

            Session.Remove(pageId);

            //if (log.isDebugEnabled()) {
            //    log.debug("Deleted page with id:" + pageId);
            //}
        }

        protected HDIVStateCache getStateCache(HttpSessionState Session)
        {

            HDIVStateCache cache = (HDIVStateCache)Session[this.cacheName];
            if (cache == null)
            {
                cache = this.createStateCacheInstance();
                Session[this.cacheName] = cache;
            }

            return cache;
        }

        protected void saveStateCache(HttpSessionState Session, HDIVStateCache stateCache)
        {

            Session[this.cacheName] = stateCache;
        }

        protected HDIVStateCache createStateCacheInstance()
        {

            HDIVStateCache cache = new HDIVStateCache();
            return cache;
        }


        protected HttpSessionState getHttpSession()
        {
            return HttpContext.Current.Session;
        }

        public void setCacheName(String cacheName)
        {
            this.cacheName = cacheName;
        }

        public void setPageIdGeneratorName(String pageIdGeneratorName)
        {
            this.pageIdGeneratorName = pageIdGeneratorName;
        }

        public void setKeyName(String keyName)
        {
            this.keyName = keyName;
        }
    }
}