﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DemoWebForms.hdiv
{
    [Serializable] public class HDIVElement
    {   
        private string type { get; set; }
        private string value { get; set; }
        private string name { get; set; }
        private string parent { get; set; }


        public HDIVElement(string iType, string iName, string iValue, string iParent)
        {
            this.type = iType;
            this.name = iName;
            this.value = iValue;
            this.parent = iParent;   
        }

        public HDIVElement(string iType, string iName, string iParent)
        {
            this.type = iType;
            this.name = iName;
            this.parent = iParent;
        }

        public HDIVElement(string iName, string iValue)
        {
            
            this.name = iName;
            this.value = iValue;
        }



        public string getType()
        {
            return this.type;
        }

        public string getName()
        {
            return this.name;
        }

        public string getValue()
        {
            return this.value;
        }

        public string getParent()
        {
            return this.parent;
        }




    }
}