﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DemoWebForms.hdiv
{
    public class PageIdGenerator
    {
        private long id;

        public PageIdGenerator()
        {
            this.id = this.generateInitialPageId();
        }

        public String getNextPageId()
        {

            this.id = this.id + 1;
            return id.ToString();
        }

        protected long generateInitialPageId()
        {

            Random r = new Random();
            int i = r.Next(20);
            return i;
        }
    }
}